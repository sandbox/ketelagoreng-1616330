<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes; ?>>

<?php
?>

	<div id="page-wrapper">
		<div id="menu-wrapper">
			<div id="background-menu">
				<div id="menu-wrapper">
					<div id="menu-bar">
						<div id="menu-bar">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="header-wrapper">
			<div id="header-shadow">
			<div class="header-wrapper">
				<div id="header">
					<div id="branding-wrapper">
						<div class="branding">
							<?php if ($logo) : ?>
								<div class="logo">
									<a href="<?php print $base_path ?>" title="<?php print t('home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('home') ?>" /></a>
								</div>
							<?php endif; ?>
							<div class="name-slogan-wrapper">
							<?php if ($site_name): ?>
								<h1 class="site-name"><a href="<?php print $base_path; ?>" title="<?php print $site_name ?>"><?php print $site_name ?></a></h1>
							<?php endif; ?>
							<?php if ($site_slogan): ?>
								<span class="site-slogan"><?php print $site_slogan; ?></span>
							<?php endif; ?>
							</div>
						</div>
					
					<div id="search-feed-and-authorize">
					
					
					<div id="authorize">
						<ul>
							<?php global $user;
								if ($user->uid !=0):
									print '<li class="first">' . "logged in as" . '<a href="' . url('user/' . $user->uid) . '">' . $user->name .  '</a></li>';
									print '<li class="last"> <a href="' . url('user/logout') . '">' . "logout" . '</a></li>';
								else :
									print '<li class="first">' . "logged is" . ' <a href="' . url('user') . '">' . "here" . '</a></li>';
									print '<li class="last"> <a href="' . url('user/register') . '">' . "register" . '</a></li>';
								endif;
							?>
						</ul>
					</div>
					</div>
					</div>
				</div>
			</div>
			</div>
				<div id="clear" style="clear:both"></div>
				
		</div>
			
			<div id="container-wrapper">
				<div class="container-wrapper">
				<div id="container-wrapper-inner">
				<div id="container-outer">
					<div id="middle-container">
						<div id="container-inner-wrapper">
							<div id="container-inner">
						
						
								<div id="main-content">
									<?php if ($show_messages): {print $messages;} endif; ?>
									<?php if ($content): ?><div class="mission"><?php print $content; ?></div><?php endif; ?>
							
								<div id="clear" style="clear:both;height:20px;"></div>
								</div>
							</div>
						</div>
					</div>
				<div id="bottom-shadow"></div>
				</div>
				</div>
				</div>
			</div>
				<div id="footer-wrapper">
					<div id="footer">
						<div id="notice" style="font-size:12px;color:#c3b0b0;margin-bottom:5px;font-style:italic;line-height:1em;"><p>Theme by<a href="http://benili93.blogspot.com">Beny Liantriana</a> and inspired by <a href="http://www.igoen.com">Bluejems</a>.</p><p>Powered by <a href="http://www.drupal.org">Drupal</a>.</p></div>
					</div>
				</div>
	</div>