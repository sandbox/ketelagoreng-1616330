<?php 
?>
	
	<div id="page-wrapper">
	<div id="menu-wrapper">
		<div id="background-menu">
			<div class="menu-wrapper">
				<div class="menu-outer">
					<div id="menu-bar">
					<?php if ($main_menu || $page['superfish_menu']): ?>
						<div id="<?php print $main_menu ? 'nav' : 'superfish'; ?>">
							<?php
							if ($main_menu) {
							print theme('links__system_main_menu', array('links' => $main_menu)); }
							elseif (!empty($page['superfish_menu'])) {
							print render ($page['superfish_menu']); }
							?>
						</div>
					<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div>
		<div id="header-wrapper">
			<div id="header-shadow">
				<div class="header-wrapper">
				<div id="header">
					<div id="branding-wrapper">
						<div class="branding">
							<?php if ($logo) : ?>
								<div class="logo">
									<a href="<?php print $base_path ?>" title="<?php print t('home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('home') ?>" /></a>
								</div>
							<?php endif; ?>
							<div class="name-slogan-wrapper">
							<?php if ($site_name): ?>
								<h1 class="site-name"><a href="<?php print $base_path; ?>" title="<?php print $site_name ?>"><?php print $site_name ?></a></h1>
							<?php endif; ?>
							<?php if ($site_slogan): ?>
								<span class="site-slogan"><?php print $site_slogan; ?></span>
							<?php endif; ?>
							</div>
						</div>
						<div id="search-feed-and-authorize">
					
					
						<div id="search-and-feed">
						
						
							<?php if ($page['search_box']): ?>
							<div id="search-box">
								<?php print render ($page['search_box']); ?>
							</div>
							<?php endif; ?>
							
							
							<?php if ($feed_icons): ?>
								<div class="feed-wrapper">
									<?php print $feed_icons; ?>
								</div>
							<?php endif; ?>
						</div>
						
						<div id="authorize">
							<ul>
								<?php global $user;
									if ($user -> uid !=0) {
										print '<li class="first">' . "logged in as " . '<a href="' . url('user/'.$user -> uid).'">'. $user->name.'</a></li>';
										print '<li class="last"><a href="' . url('user/logout').'">' . "logout".'</a></li>'; }
									else {
										print '<li class="first"><a href="'. url('user').'">'. "login". '</a></li>';
										print '<li class="last"><a href="'. url('user/register').'">'. "register". '</a></li>'; }
								?>
							</ul>
						</div>
					</div>
					</div>
				</div>
				</div>
			</div>
			<div id="clear"><div style="clear.left"></div></div>
		</div>
		
		<div id="container-wrapper">
		
		
			<div class="container-wrapper">
			<div id="sidebar-right-wrapper">
					<div class="sidebar-right-wrapper">
						<?php if ($page['sidebar_second']): ?>
						<div id="sidebar-right" class="sidebar">
							<?php print render ($page['sidebar_second']); ?>
						</div>
						<?php endif; ?>
					</div>
			</div>
			
			<div id="container-wrapper-inner">
			<div id="container-outer">
				<div id="middle-container">
				
					<?php if (!$page['slideshow']): ?>
					<?php if($is_front): ?>
					<div id="slideshow-wrapper">
						<div class="slideshow-inner">
							<?php if ($page['slideshow']): ?>
								<div id="slideshow">
									<?php print render ($page['slideshow']); ?>
								</div>
							<?php endif; ?>
							
						<div class="slideshow">
							<img src="<?php print $base_path . $directory; ?> /images/slideshow/image1.jpg" width="650" height="383" alt="image1"/>
							<img src="<?php print $base_path . $directory; ?> /images/slideshow/image2.jpg" width="650" height="383" alt="image2"/>
							<img src="<?php print $base_path . $directory; ?> /images/slideshow/image3.jpg" width="650" height="383" alt="image3"/>
						</div>
						
						
						<?php if($page['highlighted']): ?>
						<div id="mission-wrapper">
							<div class="mission-wrapper">
								<div id="mision">
									<?php print render ($page['highlighted']); ?>
								</div>
							</div>
						</div>
						<?php endif; ?>
						</div>
						<div id="slideshow-shadow"></div>
					</div>
					<?php endif; ?>
					<?php endif; ?>
					
					<?php if ($breadcrumb): ?>
						<?php if(!$is_front): ?>
							<div id="breadcrumb">
								<?php print $breadcrumb ; ?>
							</div>
						<?php endif; ?>
					<?php endif; ?>
					
					<div id="container-inner-wrapper">
					<div id="container-inner">
					
					
						<div id="main-content">
							<?php if ($page['content_top']): ?><div id="content-top"><?php print render ($page['content-top']) ;?></div><?php endif; ?>
							<?php if ($show_messages) {print $messages;} ; ?>
							<?php if ($tabs): ?><div class="tabs"><?php print render ($tabs); ?></div><?php endif; ?>
							<?php if ($title): ?><h1 class="title"><?php print render ($title_suffix); ?></h1><?php endif; ?>
							<?php print render ($page['help']); ?>
							<?php if ($page['content']): ?><?php print render ($page['content']); ?><?php endif; ?>
						</div>
						
					</div>
					</div>
					
					<div id="clear" style="clear:left"></div>
					<div id="bottom-shadow"></div>
					
					<?php if ($page['bottom_1'] || $page['bottom_2'] || $page['bottom_3'] || $page['bottom_4']): ?>
					<div id="bottom-wrapper" class="in<?php print (bool) $page['bottom_1'] + (bool) $page['bottom_2'] + (bool) $page['bottom_3'] + (bool) $page['bottom_4'] ; ?>">
						<?php if ($page['bottom_1']): ?>
							<div class="column A">
								<?php print render ($page['bottom_1']); ?>
							</div>
						<?php endif; ?>
						<?php if ($page['bottom_2']): ?>
							<div class="column B">
								<?php print render ($page['bottom_2']); ?>
							</div>
						<?php endif; ?>
						<?php if ($page['bottom_3']): ?>
							<div class="column C">
								<?php print render ($page['bottom_3']); ?>
							</div>
						<?php endif; ?>
						<?php if ($page['bottom_4']): ?>
							<div class="column D">
								<?php print render ($page['bottom_4']); ?>
							</div>
						<?php endif; ?>
					</div>
					<?php endif; ?>		
				</div>
				</div>
				</div>
					
			</div>
			</div>
		</div>
		<div id="long-bottom"><div id="clear" style="clear:both;height:30px;"></div></div>
		<div id="footer-wrapper">
			<div id="footer">
				<?php if(isset($secondary_menu)): ?>
					<div id="subnav">
						<?php print theme('links__system_secondary_menu',array('links' => $secondary_menu, 'attributes' => array('class' => array('links','clearfix')))); ?>
					</div>
				<?php endif; ?>
				<?php print render ($page['footer']); ?>
				
				<div id="notice" style="font-size:12px;color:#9ab0ef;margin-bottom:5px;font-style:italic;line-height:1em;"><p>Theme by <a href="http://benili93.blogspot.com">Beny Liantriana</a> and inspired by <a href="http://www.igoen.com">Bluejems</a>.</p><p>Powered by <a href="http://www.drupal.org">Drupal</a>.</p></div>
			</div>
		</div>
	</div>
						