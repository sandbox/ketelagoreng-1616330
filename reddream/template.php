<?php
// $Id : template.php$

if (drupal_is_front_page()) {
  drupal_add_js(drupal_get_path('theme', 'reddream') . '/scripts/jquery.cycle.all.js');
}


function reddream_preprocess_maintenance_page(&$variables) {
  if (!$variables['db_is_active']) {
    unset($variables['site_name']);
  }
  drupal_add_css(drupal_get_path('theme', 'reddream') . '/maintenance.css');
}
